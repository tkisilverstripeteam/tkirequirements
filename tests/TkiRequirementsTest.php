<?php
/**
 * This class mostly duplicates the RequirementsTest class to test
 * the TkiRequirements_Backend class, to ensure compatibility.
 * Additional methods are added to test the features made available by
 * TkiRequirements_Backend, such as prioritisation and specifying script location.
 * @package tkirequirements
 * @subpackage tests
 *
 */
class TkiRequirementsTest extends RequirementsTest {

	/*
	 * -------------------------------------------------------------------------
	 * Tests for parent methods
	 * -------------------------------------------------------------------------
	 */
	public function testExternalUrls() {
		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);

		$backend->javascript('http://www.mydomain.com/test.js');
		$backend->javascript('https://www.mysecuredomain.com/test.js');
		$backend->javascript('//scheme-relative.example.com/test.js');
		$backend->css('http://www.mydomain.com/test.css');
		$backend->css('https://www.mysecuredomain.com/test.css');
		$backend->css('//scheme-relative.example.com/test.css');

		$html = $backend->includeInHTML(false, self::$html_template);

		$this->assertTrue(
			(strpos($html, 'http://www.mydomain.com/test.js') !== false),
			'Load external javascript URL'
		);
		$this->assertTrue(
			(strpos($html, 'https://www.mysecuredomain.com/test.js') !== false),
			'Load external secure javascript URL'
		);
		$this->assertTrue(
			(strpos($html, '//scheme-relative.example.com/test.js') !== false),
			'Load external scheme-relative javascript URL'
		);
		$this->assertTrue(
			(strpos($html, 'http://www.mydomain.com/test.css') !== false),
			'Load external CSS URL'
		);
		$this->assertTrue(
			(strpos($html, 'https://www.mysecuredomain.com/test.css') !== false),
			'Load external secure CSS URL'
		);
		$this->assertTrue(
			(strpos($html, '//scheme-relative.example.com/test.css') !== false),
			'Load scheme-relative CSS URL'
		);
	}

	protected function setupCombinedRequirements($backend) {
		$basePath = $this->getCurrentRelativePath();

		$backend->clear();
		$backend->setCombinedFilesFolder('assets');

		// clearing all previously generated requirements (just in case)
		$backend->clear_combined_files();
		$backend->delete_combined_files('RequirementsTest_bc.js');

		// require files normally (e.g. called from a FormField instance)
		$backend->javascript($basePath . '/files/RequirementsTest_a.js');
		$backend->javascript($basePath . '/files/RequirementsTest_b.js');
		$backend->javascript($basePath . '/files/RequirementsTest_c.js');

		// require two of those files as combined includes
		$backend->combine_files(
			'RequirementsTest_bc.js',
			array(
				$basePath . '/files/RequirementsTest_b.js',
				$basePath . '/files/RequirementsTest_c.js'
			)
		);
	}

	protected function setupCombinedNonrequiredRequirements($backend) {
			$basePath = $this->getCurrentRelativePath();

			$backend->clear();
			$backend->setCombinedFilesFolder('assets');

			// clearing all previously generated requirements (just in case)
			$backend->clear_combined_files();
			$backend->delete_combined_files('RequirementsTest_bc.js');

			// require files as combined includes
			$backend->combine_files(
				'RequirementsTest_bc.js',
				array(
					$basePath . '/files/RequirementsTest_b.js',
					$basePath . '/files/RequirementsTest_c.js'
				)
			);
		}

	public function testCombinedJavascript() {
		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);
		$backend->setCombinedFilesFolder('assets');

		$this->setupCombinedRequirements($backend);

		$combinedFilePath = Director::baseFolder() . '/assets/' . 'RequirementsTest_bc.js';

		$html = $backend->includeInHTML(false, self::$html_template);

		/* COMBINED JAVASCRIPT FILE IS INCLUDED IN HTML HEADER */
		$this->assertTrue((bool)preg_match('/src=".*\/RequirementsTest_bc\.js/', $html),
			'combined javascript file is included in html header');

		/* COMBINED JAVASCRIPT FILE EXISTS */
		$this->assertTrue(file_exists($combinedFilePath),
			'combined javascript file exists');

		/* COMBINED JAVASCRIPT HAS CORRECT CONTENT */
		$this->assertTrue((strpos(file_get_contents($combinedFilePath), "alert('b')") !== false),
			'combined javascript has correct content');
		$this->assertTrue((strpos(file_get_contents($combinedFilePath), "alert('c')") !== false),
			'combined javascript has correct content');

		/* COMBINED FILES ARE NOT INCLUDED TWICE */
		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_b\.js/', $html),
			'combined files are not included twice');
		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_c\.js/', $html),
			'combined files are not included twice');

		/* NORMAL REQUIREMENTS ARE STILL INCLUDED */
		$this->assertTrue((bool)preg_match('/src=".*\/RequirementsTest_a\.js/', $html),
			'normal requirements are still included');

		$backend->delete_combined_files('RequirementsTest_bc.js');

		// Then do it again, this time not requiring the files beforehand
		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);
		$backend->setCombinedFilesFolder('assets');

		$this->setupCombinedNonrequiredRequirements($backend);

		$combinedFilePath = Director::baseFolder() . '/assets/' . 'RequirementsTest_bc.js';

		$html = $backend->includeInHTML(false, self::$html_template);

		/* COMBINED JAVASCRIPT FILE IS INCLUDED IN HTML HEADER */
		$this->assertTrue((bool)preg_match('/src=".*\/RequirementsTest_bc\.js/', $html),
			'combined javascript file is included in html header');

		/* COMBINED JAVASCRIPT FILE EXISTS */
		$this->assertTrue(file_exists($combinedFilePath),
			'combined javascript file exists');

		/* COMBINED JAVASCRIPT HAS CORRECT CONTENT */
		$this->assertTrue((strpos(file_get_contents($combinedFilePath), "alert('b')") !== false),
			'combined javascript has correct content');
		$this->assertTrue((strpos(file_get_contents($combinedFilePath), "alert('c')") !== false),
			'combined javascript has correct content');

		/* COMBINED FILES ARE NOT INCLUDED TWICE */
		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_b\.js/', $html),
			'combined files are not included twice');
		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_c\.js/', $html),
			'combined files are not included twice');

		$backend->delete_combined_files('RequirementsTest_bc.js');
	}

	public function testCombinedCss() {
		$basePath = $this->getCurrentRelativePath();
		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);

		$backend->combine_files(
			'print.css',
			array(
				$basePath . '/files/RequirementsTest_print_a.css',
				$basePath . '/files/RequirementsTest_print_b.css'
			),
			'print'
		);

		$html = $backend->includeInHTML(false, self::$html_template);

		$this->assertTrue((bool)preg_match('/href=".*\/print\.css/', $html), 'Print stylesheets have been combined.');
		$this->assertTrue((bool)preg_match(
			'/media="print/', $html),
			'Combined print stylesheet retains the media parameter'
		);
	}

	public function testBlockedCombinedJavascript() {
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);
		$backend->setCombinedFilesFolder('assets');
		$combinedFilePath = Director::baseFolder() . '/assets/' . 'RequirementsTest_bc.js';

		/* BLOCKED COMBINED FILES ARE NOT INCLUDED */
		$this->setupCombinedRequirements($backend);
		$backend->block('RequirementsTest_bc.js');
		$backend->delete_combined_files('RequirementsTest_bc.js');

		clearstatcache(); // needed to get accurate file_exists() results
		$html = $backend->includeInHTML(false, self::$html_template);

		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_bc\.js/', $html),
			'blocked combined files are not included ');
		$backend->unblock('RequirementsTest_bc.js');

		/* BLOCKED UNCOMBINED FILES ARE NOT INCLUDED */
		$this->setupCombinedRequirements($backend);
		$backend->block($basePath .'/files/RequirementsTest_b.js');
		$backend->delete_combined_files('RequirementsTest_bc.js');
		clearstatcache(); // needed to get accurate file_exists() results
		$html = $backend->includeInHTML(false, self::$html_template);
		$this->assertFalse((strpos(file_get_contents($combinedFilePath), "alert('b')") !== false),
			'blocked uncombined files are not included');
		$backend->unblock('RequirementsTest_b.js');

		/* A SINGLE FILE CAN'T BE INCLUDED IN TWO COMBINED FILES */
		$this->setupCombinedRequirements($backend);
		clearstatcache(); // needed to get accurate file_exists() results

		// This throws a notice-level error, so we prefix with @
		@$backend->combine_files(
			'RequirementsTest_ac.js',
			array(
				$basePath . '/files/RequirementsTest_a.js',
				$basePath . '/files/RequirementsTest_c.js'
			)
		);

		$combinedFiles = $backend->get_combine_files();
		$this->assertEquals(
			array_keys($combinedFiles),
			array('RequirementsTest_bc.js'),
			"A single file can't be included in two combined files"
		);

		$backend->delete_combined_files('RequirementsTest_bc.js');
	}

	public function testArgsInUrls() {
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);

		$backend->javascript($basePath . '/files/RequirementsTest_a.js?test=1&test=2&test=3');
		$backend->css($basePath . '/files/RequirementsTest_a.css?test=1&test=2&test=3');
		$backend->delete_combined_files('RequirementsTest_bc.js');

		$html = $backend->includeInHTML(false, self::$html_template);

		/* Javascript has correct path */
		$this->assertTrue(
			(bool)preg_match('/src=".*\/RequirementsTest_a\.js\?m=\d\d+&amp;test=1&amp;test=2&amp;test=3/',$html),
			'javascript has correct path');

		/* CSS has correct path */
		$this->assertTrue(
			(bool)preg_match('/href=".*\/RequirementsTest_a\.css\?m=\d\d+&amp;test=1&amp;test=2&amp;test=3/',$html),
			'css has correct path');
	}

	public function testRequirementsBackend() {
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend();
		$backend->javascript($basePath . '/a.js');

		$this->assertTrue(count($backend->get_javascript()) == 1,
			"There should be only 1 file included in required javascript.");
		$this->assertTrue(in_array($basePath . '/a.js', $backend->get_javascript()),
			"a.js should be included in required javascript.");

		$backend->javascript($basePath . '/b.js');
		$this->assertTrue(count($backend->get_javascript()) == 2,
			"There should be 2 files included in required javascript.");

		$backend->block($basePath . '/a.js');
		$this->assertTrue(count($backend->get_javascript()) == 1,
			"There should be only 1 file included in required javascript.");
		$this->assertFalse(in_array($basePath . '/a.js', $backend->get_javascript()),
			"a.js should not be included in required javascript after it has been blocked.");
		$this->assertTrue(in_array($basePath . '/b.js', $backend->get_javascript()),
			"b.js should be included in required javascript.");

		$backend->css($basePath . '/a.css');
		$this->assertTrue(count($backend->get_css()) == 1,
			"There should be only 1 file included in required css.");
		$this->assertArrayHasKey($basePath . '/a.css', $backend->get_css(),
			"a.css should be in required css.");

		$backend->block($basePath . '/a.css');
		$this->assertTrue(count($backend->get_css()) == 0,
			"There should be nothing in required css after file has been blocked.");
	}

	public function testConditionalTemplateRequire() {
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend();
		$holder = Requirements::backend();
		Requirements::set_backend($backend);
		$data = new ArrayData(array(
			'FailTest' => true,
		));
		$data->renderWith('TkiRequirementsTest_Conditionals');
		$this->assertFileIncluded($backend, 'css', $basePath .'/files/RequirementsTest_a.css');
		$this->assertFileIncluded($backend, 'js',
			array($basePath .'/files/RequirementsTest_b.js', $basePath .'/files/RequirementsTest_c.js'));
		$this->assertFileNotIncluded($backend, 'js', $basePath .'/files/RequirementsTest_a.js');
		$this->assertFileNotIncluded($backend, 'css',
			array($basePath .'/files/RequirementsTest_b.css', $basePath .'/files/RequirementsTest_c.css'));
		$backend->clear();
		$data = new ArrayData(array(
			'FailTest' => false,
		));
		$data->renderWith('TkiRequirementsTest_Conditionals');
		$this->assertFileNotIncluded($backend, 'css', $basePath .'/files/RequirementsTest_a.css');
		$this->assertFileNotIncluded($backend, 'js',
			array($basePath .'/files/RequirementsTest_b.js', $basePath .'/files/RequirementsTest_c.js'));
		$this->assertFileIncluded($backend, 'js', $basePath .'/files/RequirementsTest_a.js');
		$this->assertFileIncluded($backend, 'css',
			array($basePath .'/files/RequirementsTest_b.css', $basePath .'/files/RequirementsTest_c.css'));
		Requirements::set_backend($holder);
	}

	public function testJsWriteToBody() {
		$backend = new TkiRequirements_Backend();
		$backend->javascript('http://www.mydomain.com/test.js');

		// Test matching with HTML5 <header> tags as well
		$template = '<html><head></head><body><header>My header</header><p>Body</p></body></html>';

		$backend->set_write_js_to_body(false);
		$html = $backend->includeInHTML(false, $template);
		$this->assertContains('<head><script', $html);

		$backend->set_write_js_to_body(true);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotContains('<head><script', $html);
		$this->assertContains('</script></body>', $html);
	}

	public function testIncludedJsIsNotCommentedOut() {
		$template = '<html><head></head><body><!--<script>alert("commented out");</script>--></body></html>';
		$backend = new TkiRequirements_Backend();
		$backend->javascript($this->getCurrentRelativePath() . '/files/RequirementsTest_a.js');
		$html = $backend->includeInHTML(false, $template);
		//wiping out commented-out html
		$html = preg_replace('/<!--(.*)-->/Uis', '', $html);
		$this->assertContains("RequirementsTest_a.js", $html);
	}

	public function testCommentedOutScriptTagIsIgnored() {
		$template = '<html><head></head><body><!--<script>alert("commented out");</script>-->'
			. '<h1>more content</h1></body></html>';
		$backend = new TkiRequirements_Backend();
		$backend->set_suffix_requirements(false);
		$src = $this->getCurrentRelativePath() . '/files/RequirementsTest_a.js';
		$urlSrc = Controller::join_links(Director::baseURL(), $src);
		$backend->javascript($src);
		$html = $backend->includeInHTML(false, $template);
		$this->assertEquals('<html><head></head><body><!--<script>alert("commented out");</script>-->'
			. '<h1>more content</h1><script type="text/javascript" src="' . $urlSrc . '"></script></body></html>', $html);
	}

	public function testForceJsToBottom() {
		$backend = new TkiRequirements_Backend();
		$backend->javascript('http://www.mydomain.com/test.js');

		// Test matching with HTML5 <header> tags as well
		$template = '<html><head></head><body><header>My header</header><p>Body<script></script></p></body></html>';

		// The expected outputs
		$JsInHead = "<html><head><script type=\"text/javascript\" src=\"http://www.mydomain.com/test.js\">"
			. "</script>\n</head><body><header>My header</header><p>Body<script></script></p></body></html>";
		$JsInBody = "<html><head></head><body><header>My header</header><p>Body<script type=\"text/javascript\""
			. " src=\"http://www.mydomain.com/test.js\"></script><script></script></p></body></html>";
		$JsAtEnd  = "<html><head></head><body><header>My header</header><p>Body<script></script></p><script "
			. "type=\"text/javascript\" src=\"http://www.mydomain.com/test.js\"></script></body></html>";


		// Test if the script is before the head tag, not before the body.
		// Expected: $JsInHead
		$backend->set_write_js_to_body(false);
		$backend->set_force_js_to_bottom(false);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotEquals($JsInBody, $html);
		$this->assertNotEquals($JsAtEnd, $html);
		$this->assertEquals($JsInHead, $html);

		// Test if the script is before the first <script> tag, not before the body.
		// Expected: $JsInBody
		$backend->set_write_js_to_body(true);
		$backend->set_force_js_to_bottom(false);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotEquals($JsAtEnd, $html);
		$this->assertEquals($JsInBody, $html);

		// Test if the script is placed just before the closing bodytag, with write-to-body false.
		// Expected: $JsAtEnd
		$backend->set_write_js_to_body(false);
		$backend->set_force_js_to_bottom(true);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotEquals($JsInHead, $html);
		$this->assertNotEquals($JsInBody, $html);
		$this->assertEquals($JsAtEnd, $html);

		// Test if the script is placed just before the closing bodytag, with write-to-body true.
		// Expected: $JsAtEnd
		$backend->set_write_js_to_body(true);
		$backend->set_force_js_to_bottom(true);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotEquals($JsInHead, $html);
		$this->assertNotEquals($JsInBody, $html);
		$this->assertEquals($JsAtEnd, $html);
	}

	public function testSuffix() {
		$template = '<html><head></head><body><header>My header</header><p>Body</p></body></html>';
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend;

		$backend->javascript($basePath .'/files/RequirementsTest_a.js');
		$backend->javascript($basePath .'/files/RequirementsTest_b.js?foo=bar&bla=blubb');
		$backend->css($basePath .'/files/RequirementsTest_a.css');
		$backend->css($basePath .'/files/RequirementsTest_b.css?foo=bar&bla=blubb');

		$backend->set_suffix_requirements(true);
		$html = $backend->includeInHTML(false, $template);
		$this->assertRegexp('/RequirementsTest_a\.js\?m=[\d]*"/', $html);
		$this->assertRegexp('/RequirementsTest_b\.js\?m=[\d]*&amp;foo=bar&amp;bla=blubb"/', $html);
		$this->assertRegexp('/RequirementsTest_a\.css\?m=[\d]*"/', $html);
		$this->assertRegexp('/RequirementsTest_b\.css\?m=[\d]*&amp;foo=bar&amp;bla=blubb"/', $html);

		$backend->set_suffix_requirements(false);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotContains('RequirementsTest_a.js=', $html);
		$this->assertNotRegexp('/RequirementsTest_a\.js\?m=[\d]*"/', $html);
		$this->assertNotRegexp('/RequirementsTest_b\.js\?m=[\d]*&amp;foo=bar&amp;bla=blubb"/', $html);
		$this->assertNotRegexp('/RequirementsTest_a\.css\?m=[\d]*"/', $html);
		$this->assertNotRegexp('/RequirementsTest_b\.css\?m=[\d]*&amp;foo=bar&amp;bla=blubb"/', $html);
	}

	/**
	 * Ensure that if a JS snippet somewhere in the page (via requirements) contains </head> that it doesn't
	 * get injected with requirements
	 */
	public function testHeadTagIsNotInjectedTwice() {
		$template = '<html><head></head><body><header>My header</header><p>Body</p></body></html>';
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend;
		// The offending snippet that contains a closing head tag
		$backend->customScript('var myvar="<head></head>";');
		// Insert a variety of random requirements
		$backend->css($basePath .'/files/RequirementsTest_a.css');
		$backend->javascript($basePath .'/files/RequirementsTest_a.js');
		$backend->insertHeadTags('<link rel="alternate" type="application/atom+xml" title="test" href="/" />');

		// Case A: JS forced to bottom
		$backend->set_force_js_to_bottom(true);
		$this->assertContains('var myvar="<head></head>";', $backend->includeInHTML(false, $template));

		// Case B: JS written to body
		$backend->set_force_js_to_bottom(false);
		$backend->set_write_js_to_body(true);
		$this->assertContains('var myvar="<head></head>";', $backend->includeInHTML(false, $template));

		// Case C: Neither of the above
		$backend->set_write_js_to_body(false);
		$this->assertContains('var myvar="<head></head>";', $backend->includeInHTML(false, $template));
	}

	public function assertFileIncluded($backend, $type, $files) {
		$type = strtolower($type);
		switch (strtolower($type)) {
			case 'css':
				$method = 'get_css';
				$type = 'CSS';
				break;
			case 'js':
			case 'javascript':
			case 'script':
				$method = 'get_javascript';
				$type = 'JavaScript';
				break;
		}
		$includedFiles = $backend->$method();

		// Workaround for inconsistent return formats
		if($method == 'get_javascript') {
			$includedFiles = array_combine(array_values($includedFiles), array_values($includedFiles));
		}

		if(is_array($files)) {
			$failedMatches = array();
			foreach ($files as $file) {
				if(!array_key_exists($file, $includedFiles)) {
					$failedMatches[] = $file;
				}
			}
			$this->assertTrue(
				(count($failedMatches) == 0),
				"Failed asserting the $type files '"
				. implode("', '", $failedMatches)
				. "' have exact matches in the required elements:\n'"
				. implode("'\n'", array_keys($includedFiles)) . "'"
			);
		} else {
			$this->assertTrue(
				(array_key_exists($files, $includedFiles)),
				"Failed asserting the $type file '$files' has an exact match in the required elements:\n'"
				. implode("'\n'", array_keys($includedFiles)) . "'"
			);
		}
	}

	public function assertFileNotIncluded($backend, $type, $files) {
		$type = strtolower($type);
		switch ($type) {
			case 'css':
				$method = 'get_css';
				$type = 'CSS';
				break;
			case 'js':
			case 'get_javascript':
			case 'script':
				$method = 'get_javascript';
				$type = 'JavaScript';
				break;
		}
		$includedFiles = $backend->$method();

		// Workaround for inconsistent return formats
		if($method == 'get_javascript') {
			$includedFiles = array_combine(array_values($includedFiles), array_values($includedFiles));
		}

		if(is_array($files)) {
			$failedMatches = array();
			foreach ($files as $file) {
				if(array_key_exists($file, $includedFiles)) {
					$failedMatches[] = $file;
				}
			}
			$this->assertTrue(
				(count($failedMatches) == 0),
				"Failed asserting the $type files '"
				. implode("', '", $failedMatches)
				. "' do not have exact matches in the required elements:\n'"
				. implode("'\n'", array_keys($includedFiles)) . "'"
			);
		} else {
			$this->assertFalse(
				(array_key_exists($files, $includedFiles)),
				"Failed asserting the $type file '$files' does not have an exact match in the required elements:"
						. "\n'" . implode("'\n'", array_keys($includedFiles)) . "'"
			);
		}
	}

	/*
	 * -------------------------------------------------------------------------
	 * Same tests using new methods
	 * -------------------------------------------------------------------------
	 */

	public function testExternalUrlsUsinghNewMethods() {
		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);

		$backend->js('http://www.mydomain.com/test.js');
		$backend->js('https://www.mysecuredomain.com/test.js');
		$backend->js('//scheme-relative.example.com/test.js');
		$backend->stylesheets('http://www.mydomain.com/test.css');
		$backend->stylesheets('https://www.mysecuredomain.com/test.css');
		$backend->stylesheets('//scheme-relative.example.com/test.css');

		$html = $backend->includeInHTML(false, self::$html_template);

		$this->assertTrue(
			(strpos($html, 'http://www.mydomain.com/test.js') !== false),
			'Load external javascript URL'
		);
		$this->assertTrue(
			(strpos($html, 'https://www.mysecuredomain.com/test.js') !== false),
			'Load external secure javascript URL'
		);
		$this->assertTrue(
			(strpos($html, '//scheme-relative.example.com/test.js') !== false),
			'Load external scheme-relative javascript URL'
		);
		$this->assertTrue(
			(strpos($html, 'http://www.mydomain.com/test.css') !== false),
			'Load external CSS URL'
		);
		$this->assertTrue(
			(strpos($html, 'https://www.mysecuredomain.com/test.css') !== false),
			'Load external secure CSS URL'
		);
		$this->assertTrue(
			(strpos($html, '//scheme-relative.example.com/test.css') !== false),
			'Load scheme-relative CSS URL'
		);
	}

	protected function setupCombinedRequirementsUsingNewMethods($backend) {
		$basePath = $this->getCurrentRelativePath();

		$backend->clear();
		$backend->setCombinedFilesFolder('assets');

		// clearing all previously generated requirements (just in case)
		$backend->clear_combined_files();
		$backend->delete_combined_files('RequirementsTest_bc.js');

		// require files normally (e.g. called from a FormField instance)
		$backend->js($basePath . '/files/RequirementsTest_a.js');
		$backend->js($basePath . '/files/RequirementsTest_b.js');
		$backend->js($basePath . '/files/RequirementsTest_c.js');

		// require two of those files as combined includes
		$backend->extendedCombine(
			'RequirementsTest_bc.js',
			array(
				$basePath . '/files/RequirementsTest_b.js',
				$basePath . '/files/RequirementsTest_c.js'
			)
		);
	}

	protected function setupCombinedNonrequiredRequirementsUsingNewMethods($backend) {
			$basePath = $this->getCurrentRelativePath();

			$backend->clear();
			$backend->setCombinedFilesFolder('assets');

			// clearing all previously generated requirements (just in case)
			$backend->clear_combined_files();
			$backend->delete_combined_files('RequirementsTest_bc.js');

			// require files as combined includes
			$backend->extendedCombine(
				'RequirementsTest_bc.js',
				array(
					$basePath . '/files/RequirementsTest_b.js',
					$basePath . '/files/RequirementsTest_c.js'
				)
			);
		}

	public function testCombinedJavascriptUsingNewMethods() {
		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);
		$backend->setCombinedFilesFolder('assets');

		$this->setupCombinedRequirementsUsingNewMethods($backend);

		$combinedFilePath = Director::baseFolder() . '/assets/' . 'RequirementsTest_bc.js';

		$html = $backend->includeInHTML(false, self::$html_template);

		/* COMBINED JAVASCRIPT FILE IS INCLUDED IN HTML HEADER */
		$this->assertTrue((bool)preg_match('/src=".*\/RequirementsTest_bc\.js/', $html),
			'combined javascript file is included in html header');

		/* COMBINED JAVASCRIPT FILE EXISTS */
		$this->assertTrue(file_exists($combinedFilePath),
			'combined javascript file exists');

		/* COMBINED JAVASCRIPT HAS CORRECT CONTENT */
		$this->assertTrue((strpos(file_get_contents($combinedFilePath), "alert('b')") !== false),
			'combined javascript has correct content');
		$this->assertTrue((strpos(file_get_contents($combinedFilePath), "alert('c')") !== false),
			'combined javascript has correct content');

		/* COMBINED FILES ARE NOT INCLUDED TWICE */
		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_b\.js/', $html),
			'combined files are not included twice');
		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_c\.js/', $html),
			'combined files are not included twice');

		/* NORMAL REQUIREMENTS ARE STILL INCLUDED */
		$this->assertTrue((bool)preg_match('/src=".*\/RequirementsTest_a\.js/', $html),
			'normal requirements are still included');

		$backend->delete_combined_files('RequirementsTest_bc.js');

		// Then do it again, this time not requiring the files beforehand
		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);
		$backend->setCombinedFilesFolder('assets');

		$this->setupCombinedNonrequiredRequirementsUsingNewMethods($backend);

		$combinedFilePath = Director::baseFolder() . '/assets/' . 'RequirementsTest_bc.js';

		$html = $backend->includeInHTML(false, self::$html_template);

		/* COMBINED JAVASCRIPT FILE IS INCLUDED IN HTML HEADER */
		$this->assertTrue((bool)preg_match('/src=".*\/RequirementsTest_bc\.js/', $html),
			'combined javascript file is included in html header');

		/* COMBINED JAVASCRIPT FILE EXISTS */
		$this->assertTrue(file_exists($combinedFilePath),
			'combined javascript file exists');

		/* COMBINED JAVASCRIPT HAS CORRECT CONTENT */
		$this->assertTrue((strpos(file_get_contents($combinedFilePath), "alert('b')") !== false),
			'combined javascript has correct content');
		$this->assertTrue((strpos(file_get_contents($combinedFilePath), "alert('c')") !== false),
			'combined javascript has correct content');

		/* COMBINED FILES ARE NOT INCLUDED TWICE */
		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_b\.js/', $html),
			'combined files are not included twice');
		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_c\.js/', $html),
			'combined files are not included twice');

		$backend->delete_combined_files('RequirementsTest_bc.js');
	}

	public function testCombinedCssUsingNewMethods() {
		$basePath = $this->getCurrentRelativePath();
		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);

		$backend->extendedCombine(
			'print.css',
			array(
				$basePath . '/files/RequirementsTest_print_a.css',
				$basePath . '/files/RequirementsTest_print_b.css'
			),
			array('media' => 'print')
		);

		$html = $backend->includeInHTML(false, self::$html_template);

		$this->assertTrue((bool)preg_match('/href=".*\/print\.css/', $html), 'Print stylesheets have been combined.');
		$this->assertTrue((bool)preg_match(
			'/media="print/', $html),
			'Combined print stylesheet retains the media parameter'
		);
	}

	public function testBlockedCombinedJavascriptUsingNewMethods() {
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);
		$backend->setCombinedFilesFolder('assets');
		$combinedFilePath = Director::baseFolder() . '/assets/' . 'RequirementsTest_bc.js';

		/* BLOCKED COMBINED FILES ARE NOT INCLUDED */
		$this->setupCombinedRequirementsUsingNewMethods($backend);
		$backend->block('RequirementsTest_bc.js');
		$backend->delete_combined_files('RequirementsTest_bc.js');

		clearstatcache(); // needed to get accurate file_exists() results
		$html = $backend->includeInHTML(false, self::$html_template);

		$this->assertFalse((bool)preg_match('/src=".*\/RequirementsTest_bc\.js/', $html),
			'blocked combined files are not included ');
		$backend->unblock('RequirementsTest_bc.js');

		/* BLOCKED UNCOMBINED FILES ARE NOT INCLUDED */
		$this->setupCombinedRequirementsUsingNewMethods($backend);
		$backend->block($basePath .'/files/RequirementsTest_b.js');
		$backend->delete_combined_files('RequirementsTest_bc.js');
		clearstatcache(); // needed to get accurate file_exists() results
		$html = $backend->includeInHTML(false, self::$html_template);
		$this->assertFalse((strpos(file_get_contents($combinedFilePath), "alert('b')") !== false),
			'blocked uncombined files are not included');
		$backend->unblock('RequirementsTest_b.js');

		/* A SINGLE FILE CAN'T BE INCLUDED IN TWO COMBINED FILES */
		$this->setupCombinedRequirementsUsingNewMethods($backend);
		clearstatcache(); // needed to get accurate file_exists() results

		// This throws a notice-level error, so we prefix with @
		@$backend->extendedCombine(
			'RequirementsTest_ac.js',
			array(
				$basePath . '/files/RequirementsTest_a.js',
				$basePath . '/files/RequirementsTest_c.js'
			)
		);

		$combinedFiles = $backend->get_combine_files();
		$this->assertEquals(
			array_keys($combinedFiles),
			array('RequirementsTest_bc.js'),
			"A single file can't be included in two combined files"
		);

		$backend->delete_combined_files('RequirementsTest_bc.js');
	}

	public function testArgsInUrlsUsinghNewMethods() {
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend;
		$backend->set_combined_files_enabled(true);

		$backend->js($basePath . '/files/RequirementsTest_a.js?test=1&test=2&test=3');
		$backend->stylesheets($basePath . '/files/RequirementsTest_a.css?test=1&test=2&test=3');
		$backend->delete_combined_files('RequirementsTest_bc.js');

		$html = $backend->includeInHTML(false, self::$html_template);

		/* Javascript has correct path */
		$this->assertTrue(
			(bool)preg_match('/src=".*\/RequirementsTest_a\.js\?m=\d\d+&amp;test=1&amp;test=2&amp;test=3/',$html),
			'javascript has correct path');

		/* CSS has correct path */
		$this->assertTrue(
			(bool)preg_match('/href=".*\/RequirementsTest_a\.css\?m=\d\d+&amp;test=1&amp;test=2&amp;test=3/',$html),
			'css has correct path');
	}

	public function testRequirementsBackendUsinghNewMethods() {
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend();
		$backend->js($basePath . '/a.js');

		$this->assertTrue(count($backend->get_javascript()) == 1,
			"There should be only 1 file included in required javascript.");
		$this->assertTrue(in_array($basePath . '/a.js', $backend->get_javascript()),
			"a.js should be included in required javascript.");

		$backend->js($basePath . '/b.js');
		$this->assertTrue(count($backend->get_javascript()) == 2,
			"There should be 2 files included in required javascript.");

		$backend->block($basePath . '/a.js');
		$this->assertTrue(count($backend->get_javascript()) == 1,
			"There should be only 1 file included in required javascript.");
		$this->assertFalse(in_array($basePath . '/a.js', $backend->get_javascript()),
			"a.js should not be included in required javascript after it has been blocked.");
		$this->assertTrue(in_array($basePath . '/b.js', $backend->get_javascript()),
			"b.js should be included in required javascript.");

		$backend->stylesheets($basePath . '/a.css');
		$this->assertTrue(count($backend->get_css()) == 1,
			"There should be only 1 file included in required css.");
		$this->assertArrayHasKey($basePath . '/a.css', $backend->get_css(),
			"a.css should be in required css.");

		$backend->block($basePath . '/a.css');
		$this->assertTrue(count($backend->get_css()) == 0,
			"There should be nothing in required css after file has been blocked.");
	}

	public function testJsWriteToBodyUsinghNewMethods() {
		$backend = new TkiRequirements_Backend();
		$backend->js('http://www.mydomain.com/test.js');

		// Test matching with HTML5 <header> tags as well
		$template = '<html><head></head><body><header>My header</header><p>Body</p></body></html>';

		$backend->set_write_js_to_body(false);
		$html = $backend->includeInHTML(false, $template);
		$this->assertContains('<head><script', $html);

		$backend->set_write_js_to_body(true);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotContains('<head><script', $html);
		$this->assertContains('</script></body>', $html);
	}

	public function testIncludedJsIsNotCommentedOutUsinghNewMethods() {
		$template = '<html><head></head><body><!--<script>alert("commented out");</script>--></body></html>';
		$backend = new TkiRequirements_Backend();
		$backend->js($this->getCurrentRelativePath() . '/files/RequirementsTest_a.js');
		$html = $backend->includeInHTML(false, $template);
		//wiping out commented-out html
		$html = preg_replace('/<!--(.*)-->/Uis', '', $html);
		$this->assertContains("RequirementsTest_a.js", $html);
	}

	public function testCommentedOutScriptTagIsIgnoredUsinghNewMethods() {
		$template = '<html><head></head><body><!--<script>alert("commented out");</script>-->'
			. '<h1>more content</h1></body></html>';
		$backend = new TkiRequirements_Backend();
		$backend->set_suffix_requirements(false);
		$src = $this->getCurrentRelativePath() . '/files/RequirementsTest_a.js';
		$urlSrc = Controller::join_links(Director::baseURL(), $src);
		$backend->js($src);
		$html = $backend->includeInHTML(false, $template);
		$this->assertEquals('<html><head></head><body><!--<script>alert("commented out");</script>-->'
			. '<h1>more content</h1><script type="text/javascript" src="' . $urlSrc . '"></script></body></html>', $html);
	}

	public function testForceJsToBottomUsingNewMethods() {
		$backend = new TkiRequirements_Backend();
		$backend->js('http://www.mydomain.com/test.js');

		// Test matching with HTML5 <header> tags as well
		$template = '<html><head></head><body><header>My header</header><p>Body<script></script></p></body></html>';

		// The expected outputs
		$JsInHead = "<html><head><script type=\"text/javascript\" src=\"http://www.mydomain.com/test.js\">"
			. "</script>\n</head><body><header>My header</header><p>Body<script></script></p></body></html>";
		$JsInBody = "<html><head></head><body><header>My header</header><p>Body<script type=\"text/javascript\""
			. " src=\"http://www.mydomain.com/test.js\"></script><script></script></p></body></html>";
		$JsAtEnd  = "<html><head></head><body><header>My header</header><p>Body<script></script></p><script "
			. "type=\"text/javascript\" src=\"http://www.mydomain.com/test.js\"></script></body></html>";


		// Test if the script is before the head tag, not before the body.
		// Expected: $JsInHead
		$backend->set_write_js_to_body(false);
		$backend->set_force_js_to_bottom(false);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotEquals($JsInBody, $html);
		$this->assertNotEquals($JsAtEnd, $html);
		$this->assertEquals($JsInHead, $html);

		// Test if the script is before the first <script> tag, not before the body.
		// Expected: $JsInBody
		$backend->set_write_js_to_body(true);
		$backend->set_force_js_to_bottom(false);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotEquals($JsAtEnd, $html);
		$this->assertEquals($JsInBody, $html);

		// Test if the script is placed just before the closing bodytag, with write-to-body false.
		// Expected: $JsAtEnd
		$backend->set_write_js_to_body(false);
		$backend->set_force_js_to_bottom(true);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotEquals($JsInHead, $html);
		$this->assertNotEquals($JsInBody, $html);
		$this->assertEquals($JsAtEnd, $html);

		// Test if the script is placed just before the closing bodytag, with write-to-body true.
		// Expected: $JsAtEnd
		$backend->set_write_js_to_body(true);
		$backend->set_force_js_to_bottom(true);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotEquals($JsInHead, $html);
		$this->assertNotEquals($JsInBody, $html);
		$this->assertEquals($JsAtEnd, $html);
	}

	public function testSuffixUsinghNewMethods() {
		$template = '<html><head></head><body><header>My header</header><p>Body</p></body></html>';
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend;

		$backend->js($basePath .'/files/RequirementsTest_a.js');
		$backend->js($basePath .'/files/RequirementsTest_b.js?foo=bar&bla=blubb');
		$backend->stylesheets($basePath .'/files/RequirementsTest_a.css');
		$backend->stylesheets($basePath .'/files/RequirementsTest_b.css?foo=bar&bla=blubb');

		$backend->set_suffix_requirements(true);
		$html = $backend->includeInHTML(false, $template);
		$this->assertRegexp('/RequirementsTest_a\.js\?m=[\d]*"/', $html);
		$this->assertRegexp('/RequirementsTest_b\.js\?m=[\d]*&amp;foo=bar&amp;bla=blubb"/', $html);
		$this->assertRegexp('/RequirementsTest_a\.css\?m=[\d]*"/', $html);
		$this->assertRegexp('/RequirementsTest_b\.css\?m=[\d]*&amp;foo=bar&amp;bla=blubb"/', $html);

		$backend->set_suffix_requirements(false);
		$html = $backend->includeInHTML(false, $template);
		$this->assertNotContains('RequirementsTest_a.js=', $html);
		$this->assertNotRegexp('/RequirementsTest_a\.js\?m=[\d]*"/', $html);
		$this->assertNotRegexp('/RequirementsTest_b\.js\?m=[\d]*&amp;foo=bar&amp;bla=blubb"/', $html);
		$this->assertNotRegexp('/RequirementsTest_a\.css\?m=[\d]*"/', $html);
		$this->assertNotRegexp('/RequirementsTest_b\.css\?m=[\d]*&amp;foo=bar&amp;bla=blubb"/', $html);
	}

	/**
	 * Ensure that if a JS snippet somewhere in the page (via requirements) contains </head> that it doesn't
	 * get injected with requirements
	 */
	public function testHeadTagIsNotInjectedTwiceUsinghNewMethods() {
		$template = '<html><head></head><body><header>My header</header><p>Body</p></body></html>';
		$basePath = $this->getCurrentRelativePath();

		$backend = new TkiRequirements_Backend;
		// The offending snippet that contains a closing head tag
		$backend->customScript('var myvar="<head></head>";');
		// Insert a variety of random requirements
		$backend->stylesheets($basePath .'/files/RequirementsTest_a.css');
		$backend->js($basePath .'/files/RequirementsTest_a.js');
		$backend->insertHeadTags('<link rel="alternate" type="application/atom+xml" title="test" href="/" />');

		// Case A: JS forced to bottom
		$backend->set_force_js_to_bottom(true);
		$this->assertContains('var myvar="<head></head>";', $backend->includeInHTML(false, $template));

		// Case B: JS written to body
		$backend->set_force_js_to_bottom(false);
		$backend->set_write_js_to_body(true);
		$this->assertContains('var myvar="<head></head>";', $backend->includeInHTML(false, $template));

		// Case C: Neither of the above
		$backend->set_write_js_to_body(false);
		$this->assertContains('var myvar="<head></head>";', $backend->includeInHTML(false, $template));
	}

	/*
	 * -------------------------------------------------------------------------
	 * Tests for new metnods
	 * -------------------------------------------------------------------------
	 */

	protected static function access_method($name)
	{
		$class = new ReflectionClass('TkiRequirements_Backend');
		$method = $class->getMethod($name);
		$method->setAccessible(true);
		return $method;
	}

	public function testGetFileInfo()
	{
		$backend = new TkiRequirements_Backend;
		$getFileInfoMethod = self::access_method('getFileInfo');

		/*
		 * 'css' type
		 */
		// Test string
		$file = 'file1.css';
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file1.css',$result['path'],'File path value contains file name');
		$this->assertEquals('css',$result['ext'],'File ext value contains correct extension');

		// Test associative array
		$file = array('path' => 'file2.css', 'ext' => 'css');
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file2.css',$result['path'],'File path value contains file name');
		$this->assertEquals('css',$result['ext'],'File ext value contains correct extension');


		// Test numeric array
		$file = array('file3.css','css');
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file3.css',$result['path'],'File path value contains file name');
		$this->assertEquals('css',$result['ext'],'File ext value contains correct extension');

		// Test array with path only
		$file = array('file4.css');
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file4.css',$result['path'],'File path value contains file name');
		$this->assertEquals('css',$result['ext'],'File ext value contains correct extension');

		/*
		 * 'js' type
		 */
		// Test string
		$file = 'file1.js';
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file1.js',$result['path'],'File path value contains file name');
		$this->assertEquals('js',$result['ext'],'File ext value contains correct extension');

		// Test associative array
		$file = array('path' => 'file2.js', 'ext' => 'js');
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file2.js',$result['path'],'File path value contains file name');
		$this->assertEquals('js',$result['ext'],'File ext value contains correct extension');


		// Test numeric array
		$file = array('file3.js','js');
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file3.js',$result['path'],'File path value contains file name');
		$this->assertEquals('js',$result['ext'],'File ext value contains correct extension');

		// Test array with path only
		$file = array('file4.js');
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file4.js',$result['path'],'File path value contains file name');
		$this->assertEquals('js',$result['ext'],'File ext value contains correct extension');

		/*
		 * 'javascript' type
		 */

		// Test associative array
		$file = array('path' => 'file1.js', 'ext' => 'javascript');
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file1.js',$result['path'],'File path value contains file name');
		$this->assertEquals('js',$result['ext'],'File ext value contains correct extension');


		// Test numeric array
		$file = array('file2.js','javascript');
		$result = $getFileInfoMethod->invokeArgs($backend,array($file));
		$this->assertTrue((is_array($result) && count($result) === 2),'File info is an array with two items');
		$this->assertEquals('file2.js',$result['path'],'File path value contains file name');
		$this->assertEquals('js',$result['ext'],'File ext value contains correct extension');

	}

	public function testSeparateFilesByType()
	{
		$backend = new TkiRequirements_Backend;
		$basePath = $this->getCurrentRelativePath();
		$separateFilesByTypeMethod = self::access_method('separateFilesByType');

		/*
		 * Non-combined requirements
		 */
		$files = $this->setupMultipleNonCombinedRequirements();
		$separated = $separateFilesByTypeMethod->invokeArgs($backend,array($files));
		// Test CSS files were separated
		$this->assertArrayHasKey('css',$separated, 'Separated files array has "css" key');
		$arrayDiff = array_diff(array(
			$basePath . '/files/RequirementsTest_a.css',
			$basePath . '/files/RequirementsTest_b.css',
			$basePath . '/files/RequirementsTest_c.css'
		),$separated['css']);
		$this->assertTrue(empty($arrayDiff),'Separated array has all required CSS files');

		// Test JavaScript files were separated
		$this->assertArrayHasKey('js',$separated, 'Separated files array has "js" key');
		$arrayDiff = array_diff(array(
			$basePath . '/files/RequirementsTest_a.js',
			$basePath . '/files/RequirementsTest_b.js',
			$basePath . '/files/RequirementsTest_c.js'
		),$separated['js']);
		$this->assertTrue(empty($arrayDiff),'Separated array has all required JS files');

	}

	public function testRequireFiles()
	{
		$backend = new TkiRequirements_Backend;
		$basePath = $this->getCurrentRelativePath();
		$backend->setCombinedFilesFolder('assets');

		/*
		 * Non-combined
		 */

		// Test different formats allowed for $files argument
		$backend->requireFiles($this->setupMultipleNonCombinedRequirements());

		$html = $backend->includeInHTML(false, self::$html_template);

		$this->assertContains("RequirementsTest_a.css", $html, 'Stylesheet A included');
		$this->assertContains("RequirementsTest_b.css", $html, 'Stylesheet B included');
		$this->assertContains("RequirementsTest_c.css", $html, 'Stylesheet C included');
		$this->assertContains("RequirementsTest_a.js", $html, 'Script A included');
		$this->assertContains("RequirementsTest_b.js", $html, 'Script B included');
		$this->assertContains("RequirementsTest_c.js", $html, 'Script C included');

		$backend->clear();

		/*
		 * Combined
		 */

		// Test different formats allowed for $files argument
		$backend->requireFiles($this->setupMultipleCombinedRequirements());

		$html = $backend->includeInHTML(false, self::$html_template);

		$this->assertContains("combined_css_a.css", $html, 'Combined CSS a included');
		$this->assertNotContains("RequirementsTest_a.css", $html, 'Stylesheet A not linked');

		$this->assertContains("combined_css_bc.css", $html, 'Combined CSS bc included');
		$this->assertNotContains("RequirementsTest_b.css", $html, 'Stylesheet B not linked');
		$this->assertNotContains("RequirementsTest_c.css", $html, 'Stylesheet C not linked');

		$this->assertContains("combined_css_de.css", $html, 'Combined CSS de included');
		$this->assertNotContains("RequirementsTest_d.css", $html, 'Stylesheet D not linked');
		$this->assertNotContains("RequirementsTest_e.css", $html, 'Stylesheet E not linked');

		$this->assertContains("combined_js_ab.js", $html, 'Combined JS ab included');
		$this->assertNotContains("RequirementsTest_a.js", $html, 'Script A not linked');
		$this->assertNotContains("RequirementsTest_b.js", $html, 'Script B not linked');

		$this->assertContains("combined_js_cd.js", $html, 'Combined JS cd included');
		$this->assertNotContains("RequirementsTest_c.js", $html, 'Script C not linked');
		$this->assertNotContains("RequirementsTest_d.js", $html, 'Script D not linked');

		$this->assertContains("combined_js_e.js", $html, 'Combined JS e included');
		$this->assertNotContains("RequirementsTest_e.js", $html, 'Script E not linked');

		$this->assertContains("combined_mixed_f.css", $html, 'Combined CSS f included');
		$this->assertContains("combined_mixed_f.js", $html, 'Combined JS f included');
		$this->assertNotContains("RequirementsTest_f.css", $html, 'Stylesheet F not linked');
		$this->assertNotContains("RequirementsTest_f.js", $html, 'Script F not linked');

		$backend->clear();
		$backend->clear_combined_files();
		$backend->delete_combined_files('combined_css_a.css');
		$backend->delete_combined_files('combined_css_bc.css');
		$backend->delete_combined_files('combined_css_de.css');
		$backend->delete_combined_files('combined_js_ab.js');
		$backend->delete_combined_files('combined_js_cd.js');
		$backend->delete_combined_files('combined_js_e.js');
		$backend->delete_combined_files('combined_mixed_f.css');
		$backend->delete_combined_files('combined_mixed_f.js');
	}

	protected function setupMultipleCombinedRequirements()
	{
		$basePath = $this->getCurrentRelativePath();

		return array(
			// Combine key and string value
			'combined_css_a' => $basePath . '/files/RequirementsTest_a.css',
			// Combine key and array of simple path values
			'combined_css_bc' => array(
				$basePath . '/files/RequirementsTest_b.css',
				$basePath . '/files/RequirementsTest_c.css'
			),
			// Associative array with path value
			'combined_css_de' => array(
				array('path' => $basePath . '/files/RequirementsTest_d.css'),
				array('path' => $basePath . '/files/RequirementsTest_e.css')
			),
			// Combine key and nested numeric array with path value
			'combined_js_ab' => array(
				array($basePath . '/files/RequirementsTest_a.js'),
				array($basePath . '/files/RequirementsTest_b.js')
			),
			// Combine key and nested numeric array with path and type values
			'combined_js_cd' => array(
				array($basePath . '/files/RequirementsTest_c.js', 'js'),
				array($basePath . '/files/RequirementsTest_d.js', 'js')
			),
			// As above, with javascript key
			'combined_js_e' => array(array($basePath . '/files/RequirementsTest_e.js', 'javascript')),
			// Mixed
			'combined_mixed_f' => array(
				$basePath . '/files/RequirementsTest_f.css',
				$basePath . '/files/RequirementsTest_f.js'
			)
		);

	}

	protected function setupMultipleNonCombinedRequirements()
	{
		$basePath = $this->getCurrentRelativePath();

		return array(
			// String path
			$basePath . '/files/RequirementsTest_a.css',
			// Numeric array with path value
			array($basePath . '/files/RequirementsTest_b.css'),
			// Associative array with path value
			array('path' => $basePath . '/files/RequirementsTest_c.css'),
			// Numeric array with path and type values
			array($basePath . '/files/RequirementsTest_a.js','js'),
			// Associative array with path and type keys and values
			array('path' => $basePath . '/files/RequirementsTest_b.js','type' => 'js'),
			// As above, with javascript type
			array('path' => $basePath . '/files/RequirementsTest_c.js','type' => 'javascript')
		);

	}
	/*
	 * -------------------------------------------------------------------------
	 * Tests for new features
	 * -------------------------------------------------------------------------
	 */

	public function testCSSPrioritisation() {
		$basePath = $this->getCurrentRelativePath();
		$backend = new TkiRequirements_Backend;
		Config::inst()->update('TkiRequirements_Backend','default_priority',50);	// Set default
		/*
		 * Uncombined
		 */

		/* Without prioritisation */
		$backend->set_combined_files_enabled(false);
		// Single requirement
		$backend->stylesheets($basePath . '/files/RequirementsTest_a.css');
		// Multiple requirement
		$backend->stylesheets(array(
			$basePath . '/files/RequirementsTest_b.css',
			$basePath . '/files/RequirementsTest_c.css')
		);

		$html = $backend->includeInHTML(false, self::$html_template);

		$posA = strpos($html,'RequirementsTest_a.css');
		$posB = strpos($html,'RequirementsTest_b.css');
		$posC = strpos($html,'RequirementsTest_c.css');

		$this->assertTrue(($posA !== false), 'Stylesheet A included');
		$this->assertTrue(($posB !== false), 'Stylesheet B included');
		$this->assertTrue(($posC !== false), 'Stylesheet C included');

		$this->assertTrue((($posA < $posB) && ($posB < $posC)),
			'Unprioritised stylesheets loaded in order required'
		);

		/* With prioritisation */
		$backend->clear();

		// Single requirement
		$backend->stylesheets($basePath . '/files/RequirementsTest_a.css',null,20);
		$backend->stylesheets($basePath . '/files/RequirementsTest_b.css');	// Default priority (50)
		//
		// Multiple requirements
		$backend->stylesheets(array(
			$basePath . '/files/RequirementsTest_c.css',
			$basePath . '/files/RequirementsTest_d.css'
		),null,70);


		$html = $backend->includeInHTML(false, self::$html_template);

		$posA = strpos($html,'RequirementsTest_a.css');
		$posB = strpos($html,'RequirementsTest_b.css');
		$posC = strpos($html,'RequirementsTest_c.css');
		$posD = strpos($html,'RequirementsTest_d.css');

		$this->assertTrue(($posA !== false), 'Stylesheet A included');
		$this->assertTrue(($posB !== false), 'Stylesheet B included');
		$this->assertTrue(($posC !== false), 'Stylesheet C included');
		$this->assertTrue(($posD !== false), 'Stylesheet D included');

		// Order should be: c, d, b, a
		$this->assertTrue((($posC < $posD) && ($posD < $posB) && ($posB < $posA)),
			'Prioritised stylesheets loaded in order specified'
		);

		$backend->clear();

		/*
		 * Combined
		 */

		$backend->set_combined_files_enabled(true);

		/* Without prioritisation */
		// Single requirement
		$backend->stylesheets($basePath . '/files/RequirementsTest_a.css');
		$backend->stylesheets($basePath . '/files/RequirementsTest_b.css');

		// Multiple requirements
		$backend->stylesheets(array(
			'RequirementsTest_cd' => array(
				$basePath . '/files/RequirementsTest_c.css',
				$basePath . '/files/RequirementsTest_d.css'
			)
		));

		$html = $backend->includeInHTML(false, self::$html_template);

		$posA = strpos($html,'RequirementsTest_a.css');
		$posB = strpos($html,'RequirementsTest_b.css');
		$posCD = strpos($html,'RequirementsTest_cd.css');

		$this->assertTrue(($posA !== false), 'Stylesheet A linked');
		$this->assertTrue(($posB !== false), 'Stylesheet B linked');
		$this->assertNotContains('RequirementsTest_c.css',$html,'Stylesheet C not linked');
		$this->assertNotContains('RequirementsTest_d.css',$html,'Stylesheet D not linked');
		$this->assertTrue(($posCD !== false), 'Combined stylesheet CD linked');

		// Order should be: a, b, cd
		$this->assertTrue((($posA < $posB) && ($posB < $posCD)),
			'Unprioritised combined stylesheets loaded in order required'
		);

		$backend->clear();
		$backend->clear_combined_files();
		$backend->delete_combined_files('RequirementsTest_cd.css');

		/* With prioritisation */

		// Single requirement
		$backend->stylesheets($basePath . '/files/RequirementsTest_a.css',null,30);
		$backend->stylesheets($basePath . '/files/RequirementsTest_b.css');	// Default priority
		//
		// Multiple requirements
		$backend->stylesheets(array(
			'RequirementsTest_cd' => array(
				$basePath . '/files/RequirementsTest_c.css',
				$basePath . '/files/RequirementsTest_d.css'
			)
		),null,70);
		// Single requirement
		$html = $backend->includeInHTML(false, self::$html_template);

		$posA = strpos($html,'RequirementsTest_a.css');
		$posB = strpos($html,'RequirementsTest_b.css');
		$posCD = strpos($html,'RequirementsTest_cd.css');

		$this->assertTrue(($posA !== false), 'Stylesheet A linked');
		$this->assertTrue(($posB !== false), 'Stylesheet B linked');
		$this->assertNotContains('RequirementsTest_c.css',$html,'Stylesheet C not linked');
		$this->assertNotContains('RequirementsTest_d.css',$html,'Stylesheet D not linked');
		$this->assertTrue(($posCD !== false), 'Combined stylesheet CD linked');

		// Order should be: cd, b, a
		$this->assertTrue((($posCD < $posB)	&& ($posB < $posA)),
			'Prioritised combined stylesheets loaded in order specified'
		);

		$backend->clear();
		$backend->clear_combined_files();
		$backend->delete_combined_files('RequirementsTest_cd.css');
	}

	public function testJSPrioritisation() {
		$basePath = $this->getCurrentRelativePath();
		$backend = new TkiRequirements_Backend;
		Config::inst()->update('TkiRequirements_Backend','default_priority',50);	// Set default
		/*
		 * Uncombined
		 */

		/* Without prioritisation */
		$backend->set_combined_files_enabled(false);
		// Single requirement
		$backend->js($basePath . '/files/RequirementsTest_a.js');
		// Multiple requirement
		$backend->js(array(
			$basePath . '/files/RequirementsTest_b.js',
			$basePath . '/files/RequirementsTest_c.js')
		);

		$html = $backend->includeInHTML(false, self::$html_template);

		$posA = strpos($html,'RequirementsTest_a.js');
		$posB = strpos($html,'RequirementsTest_b.js');
		$posC = strpos($html,'RequirementsTest_c.js');

		$this->assertTrue(($posA !== false), 'Script A included');
		$this->assertTrue(($posB !== false), 'Script B included');
		$this->assertTrue(($posC !== false), 'Script C included');

		$this->assertTrue((($posA < $posB) && ($posB < $posC)),
			'Unprioritised scripts loaded in order required'
		);

		/* With prioritisation */
		$backend->clear();

		// Single requirement
		$backend->js($basePath . '/files/RequirementsTest_a.js',null,30);
		$backend->js($basePath . '/files/RequirementsTest_b.js');	// Default priority (50)
		//
		// Multiple requirements
		$backend->js(array(
			$basePath . '/files/RequirementsTest_c.js',
			$basePath . '/files/RequirementsTest_d.js'
		),null,70);


		$html = $backend->includeInHTML(false, self::$html_template);

		$posA = strpos($html,'RequirementsTest_a.js');
		$posB = strpos($html,'RequirementsTest_b.js');
		$posC = strpos($html,'RequirementsTest_c.js');
		$posD = strpos($html,'RequirementsTest_d.js');

		$this->assertTrue(($posA !== false), 'Script A included');
		$this->assertTrue(($posB !== false), 'Script B included');
		$this->assertTrue(($posC !== false), 'Script C included');
		$this->assertTrue(($posD !== false), 'Script D included');

		// Order should be: c, d, b, a
		$this->assertTrue((($posC < $posD) && ($posD < $posB) && ($posB < $posA)),
			'Prioritised scripts loaded in order specified'
		);

		$backend->clear();

		/*
		 * Combined
		 */

		$backend->set_combined_files_enabled(true);

		/* Without prioritisation */
		// Single requirement
		$backend->js($basePath . '/files/RequirementsTest_a.js');
		$backend->js($basePath . '/files/RequirementsTest_b.js');

		// Multiple requirements
		$backend->js(array(
			'RequirementsTest_cd' => array(
				$basePath . '/files/RequirementsTest_c.js',
				$basePath . '/files/RequirementsTest_d.js'
			)
		));

		$html = $backend->includeInHTML(false, self::$html_template);

		$posA = strpos($html,'RequirementsTest_a.js');
		$posB = strpos($html,'RequirementsTest_b.js');
		$posCD = strpos($html,'RequirementsTest_cd.js');

		$this->assertTrue(($posA !== false), 'Script A linked');
		$this->assertTrue(($posB !== false), 'Script B linked');
		$this->assertNotContains('RequirementsTest_c.js',$html,'Script C not linked');
		$this->assertNotContains('RequirementsTest_d.js',$html,'Script D not linked');
		$this->assertTrue(($posCD !== false), 'Combined script CD linked');

		// Order should be: a, b, cd
		$this->assertTrue((($posA < $posB) && ($posB < $posCD)),
			'Unprioritised combined scripts loaded in order required'
		);

		$backend->clear();
		$backend->clear_combined_files();
		$backend->delete_combined_files('RequirementsTest_cd.js');

		/* With prioritisation */

		// Single requirement
		$backend->js($basePath . '/files/RequirementsTest_a.js',null,30);
		$backend->js($basePath . '/files/RequirementsTest_b.js');	// Default priority
		//
		// Multiple requirements
		$backend->js(array(
			'RequirementsTest_cd' => array(
				$basePath . '/files/RequirementsTest_c.js',
				$basePath . '/files/RequirementsTest_d.js'
			)
		),null,70);
		// Single requirement
		$html = $backend->includeInHTML(false, self::$html_template);

		$posA = strpos($html,'RequirementsTest_a.js');
		$posB = strpos($html,'RequirementsTest_b.js');
		$posCD = strpos($html,'RequirementsTest_cd.js');

		$this->assertTrue(($posA !== false), 'Script A linked');
		$this->assertTrue(($posB !== false), 'Script B linked');
		$this->assertNotContains('RequirementsTest_c.js',$html,'Script C not linked');
		$this->assertNotContains('RequirementsTest_d.js',$html,'Script D not linked');
		$this->assertTrue(($posCD !== false), 'Combined script CD linked');

		// Order should be: cd, b, a
		$this->assertTrue((($posCD < $posB)	&& ($posB < $posA)),
			'Prioritised combined scripts loaded in order specified'
		);

		$backend->clear();
		$backend->clear_combined_files();
		$backend->delete_combined_files('RequirementsTest_cd.js');
	}

	protected function setupRequirementsForJsLocation($backend) {
		$backend->clear();

		// Require JS without location and priority (use defaults)
		$backend->js('http://www.mydomain.com/test_c.js');

		// Require JS with location and without priority // Default priority (50)
		$backend->js('http://www.mydomain.com/test_d.js', 'bottom');

		// Require JS with location and priority
		$backend->js('http://www.mydomain.com/test_b.js','body', 60);

		// Require JS with location and priority
		$backend->js('http://www.mydomain.com/test_a.js', 'head', 70);

	}

	public function testSpecifyingJsLocation() {
		$backend = new TkiRequirements_Backend();

		/*
		 * Setup
		 */
		// Test matching with HTML5 <header> tags as well
		$template = '<html><head></head><body><header>My header</header><p>Body<script></script></p></body></html>';

		$jsInHead = "<html><head>"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_a.js\"></script>\n"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_b.js\"></script>\n"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_c.js\"></script>\n"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_d.js\"></script>\n"
			. "</head><body><header>My header</header><p>Body<script></script></p></body></html>";

		$jsInAllLocations = "<html><head>"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_a.js\"></script>\n"
			. "</head><body><header>My header</header><p>Body"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_b.js\"></script>"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_c.js\"></script>"
			. "<script></script></p>"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_d.js\"></script>"
			. "</body></html>";

		$jsAtBottom  = "<html><head></head><body><header>My header</header><p>Body<script></script></p>"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_a.js\"></script>"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_b.js\"></script>"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_c.js\"></script>"
			. "<script type=\"text/javascript\" src=\"http://www.mydomain.com/test_d.js\"></script>"
			. "</body></html>";

		$this->setupRequirementsForJsLocation($backend);

		/*
		 * Test JS forced into head
		 * Expected: $jsInHead
		 */

		// Test all scripts moved into head before the closing head tag, and maintain
		// prioritisation.
		$backend->set_write_js_to_body(false);
		$backend->set_force_js_to_bottom(false);

		$html = $backend->includeInHTML(false, $template);

		$this->assertNotEquals($jsInAllLocations, $html);
		$this->assertNotEquals($jsAtBottom, $html);
		$this->assertEquals($jsInHead, $html);

		/*
		 * Test JS in all locations
		 * Expected: $jsInAllLocations
		 */

		// Test one script in head before the closing head tag,
		// and two scripts in body before first <script> tag,
		// and one script at the bottom.
		$backend->set_write_js_to_body(true);
		$backend->set_force_js_to_bottom(false);

		$html = $backend->includeInHTML(false, $template);

		$this->assertNotEquals($jsInHead, $html);
		$this->assertNotEquals($jsAtBottom, $html);
		$this->assertEquals($jsInAllLocations, $html);

		/*
		 * Test JS forced to end of body
		 * Expected: $jsAtBottom
		 */

		// Test all scripts placed just before the closing body tag, and maintain
		// prioritisation. (write_js_to_body = false)

		$backend->set_write_js_to_body(false);
		$backend->set_force_js_to_bottom(true);

		$html = $backend->includeInHTML(false, $template);

		$this->assertNotEquals($jsInHead, $html);
		$this->assertNotEquals($jsInAllLocations, $html);
		$this->assertEquals($jsAtBottom, $html);

		// Test all scripts placed just before the closing body tag, and maintain
		// prioritisation. (write_js_to_body = true)
		$backend->set_write_js_to_body(true);
		$backend->set_force_js_to_bottom(true);

		$html = $backend->includeInHTML(false, $template);

		$this->assertNotEquals($jsInHead, $html);
		$this->assertNotEquals($jsInAllLocations, $html);
		$this->assertEquals($jsAtBottom, $html);
	}

	protected function setupMixedRequirements($backend)
	{
		$basePath = $this->getCurrentRelativePath();

		/*
		 * CSS
		 */
		// Combined css
		$backend->stylesheets(array(
			'RequirementsTest_de' => array(
				$basePath . '/files/RequirementsTest_d.css',
				$basePath . '/files/RequirementsTest_e.css'
			),
		),null,30);

		// Associative array with path value - Default priority
		$backend->stylesheets(array(
			array('path' => $basePath . '/files/RequirementsTest_c.css')
		));

		// // Numeric array with path and type values
		$backend->stylesheets(array(
			array($basePath . '/files/RequirementsTest_b.css')
		),null,70);


		/*
		 * JS
		 */

		// Associative array with path value
		$backend->js(array(
			array('path' => $basePath . '/files/RequirementsTest_e.js')
		),null,30);

		// Combined js - Default priority
		$backend->js(array(
			'RequirementsTest_cd' => array(
				$basePath . '/files/RequirementsTest_c.js',
				$basePath . '/files/RequirementsTest_d.js'
			)
		));

		// Numeric array with path and type values
		$backend->js(array(
			array($basePath . '/files/RequirementsTest_b.js')
		),'head',70);

		/*
		 * Mixed
		 */

		// String path
		$backend->requireFiles(array(
			$basePath . '/files/RequirementsTest_a.css',
			$basePath . '/files/RequirementsTest_a.js'
		),array('priority' => 70));

		/*
		 * Output order should be the following...
		 * CSS: a, b, c, de
		 * JS: a, b, cd, e
		 */
	}
}
