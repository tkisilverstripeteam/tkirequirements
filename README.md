# Customised SilverStripe Requirements Backend

This module provides a customised backend for the SilverStripe Requirements class. The custom class adds the features mentioned below, while maintaining compatibility with the existing Requirements_Backend class (which it subclasses from).

## Features

### Multiple files
Multiple files can be required with one call, instead of having to make a call for each and every file required.

For more information, please refer to ["Multiple files"](https://bitbucket.org/tkisilverstripeteam/tkirequirements/src/master/docs/en/index.md#markdown-header-multiple-files) feature in the documentation.

### Prioritisation
Perhaps you may have experienced difficulties getting your JavaScript and third-party libraries loading in the correct order? The first required, first included approach of the existing Requirements backend can pose a challenge, especially with various requirement calls throughout the code and modules. This module aims to solve that by allowing developers to specify a "priority" for each required file or group of files.

For more information, please refer to the  ["Prioritisation"](https://bitbucket.org/tkisilverstripeteam/tkirequirements/src/master/docs/en/index.md#markdown-header-prioritisation) feature in the documentation.

### Script location
Perhaps you have tried to load an essential script or two in the document head, while loading the rest of the JavaScript near the end of the document body, but couldn't? You may have had to insert it in your template to make it work. This module solves that by allowing developers to specify a "location" for each required file or group of files, instead having them lumped all together in one location.

For more information, please refer to the ["Script location"](https://bitbucket.org/tkisilverstripeteam/tkirequirements/src/master/docs/en/index.md#markdown-header-script-location) feature in the documentation.


## More planned features
The simple prioritisation is a start, but perhaps we can do better. Hopefully we'll find the time to add the following features soon:
* SilverStripe 4.x compatibility
* Script registration and queuing - Registering scripts in the configuration, identified by a unique name. These can then be required easily by name. eg. Requirements::backend()->requireByName('jquery');
* Specifying script dependencies
