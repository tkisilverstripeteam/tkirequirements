<?php


class TkiRequirementsExamples extends Controller {

  private static $allowed_actions = array(
		'examples'
	);

  protected $templates = array('TkiRequirementsExamples');

  public function examples(SS_HTTPRequest $request) {
    if(!Permission::check('ADMIN')) {
        return $this->httpError(403);
    }
    $param = $request->param('ID');
    if(!is_numeric($param)) {
      return sprintf('Invalid parameter "%s". Please specify a number.',$param);
    }
    $param = (int) $param;
    $backend = Requirements::backend();
    $filePath = 'tkirequirements/tests/files';

    switch($param) {
      case 1:
      $backend->stylesheets(
        array(
          "$filePath/RequirementsTest_a.css",
          "$filePath/RequirementsTest_b.css"
        ),
        'screen'
      );
        break;
      case 2:
        $backend->js(
          array(
            "$filePath/RequirementsTest_a.js",
            "$filePath/RequirementsTest_b.js"
          )
        );
        break;
      case 3:
        $backend->requireFiles(
          array(
            "$filePath/RequirementsTest_a.css",
            "$filePath/RequirementsTest_a.js",
            "$filePath/RequirementsTest_b.js"
          )
        );
        break;
      case 4:
        $_REQUEST['combine'] = 1; // Ensure combining happens in dev environment
        $backend->requireFiles(array(
          'example2' => array(
              "$filePath/RequirementsTest_a.css",
              "$filePath/RequirementsTest_a.js",
              "$filePath/RequirementsTest_b.js"
            )
          )
        );
        break;
      case 5:
        $backend->js("$filePath/RequirementsTest_a.js", 'head');

        $backend->js("$filePath/RequirementsTest_b.js", 'body');

        $backend->js(array(
          "$filePath/RequirementsTest_c.js",
          "$filePath/RequirementsTest_d.js"
        ), 'bottom');

        break;
      case 6:
        // JS vars in head
        $backend->script("var A ='foo'; var B = 'bar';",null,'head');

        // JS Libs
        $backend->js(array(
          "$filePath/RequirementsTest_b.js",
          "$filePath/RequirementsTest_c.js"
        ),'body');

        // JS script at bottom
        $backend->script("alert(A + B)", null, 'bottom');
        break;
      case 7:
        // Using stock method - assigned default priority (50)
        $backend->javascript("$filePath/RequirementsTest_c.js");
        // Page script to be loaded after libs
        $backend->js("$filePath/RequirementsTest_d.js", 'body', 30);
        // JS Libs
        $backend->js(array(
          "$filePath/RequirementsTest_a.js",
          "$filePath/RequirementsTest_b.js"
        ), 'body', 70);
        // Scripts ordered a,b,c,d in body location
        break;
      case 8:
        // Page script to be loaded after libs
        $backend->script("alert(A + B);", 'example8', 'bottom', 30);

        // Body JS Libs
        $backend->js(array(
          "$filePath/RequirementsTest_b.js",
          "$filePath/RequirementsTest_c.js"
        ), 'bottom', 40);

        // Head JS libs
        $backend->js("$filePath/RequirementsTest_a.js", 'head', 70);

        // Head JS vars
        $backend->script("var A ='foo'; var B = 'bar';",null,'head',80);

        break;
      case 9:
        $backend->stylesheets("$filePath/RequirementsTest_a.css",'screen');
        $backend->customCSS('body { background-color: #f00; }');
        $backend->css("$filePath/RequirementsTest_b.php");
        break;

    }
    $response = new SS_HTTPResponse;
    if(empty($_GET['html'])) {
      $response->addHeader('Content-Type', 'text/plain');
    }

    $data = new ArrayData(array('number' => $param));
    $response->setBody($this->render($data));
    $this->setResponse($response);
    return $this->getResponse();
  }



}
