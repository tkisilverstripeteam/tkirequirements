# Documentation #

## New methods

New public methods and arguments are introduced in the TkiRequirements_Backend class to provide the features described in the sections below.

### JavaScript and CSS files
``` php
// Use in place of Requirements::css() method
public function stylesheets($files, $media = null, $priority = null);
```

``` php
// Use in place of Requirements::javascript() method
public function js($files, $location = null, $priority = null);
```

``` php
public function requireFiles($files,$options=array());
```

### Script and style blocks
``` php
// Use in place of Requirements::customScript() method
public function script($script, $uniquenessID = null, $location = null, $priority = null);
```

``` php
// Use in place of Requirements::customCSS() method
public function styles($styles, $uniquenessID = null, $media = null);

```

 Note that the "stylesheets" and "js" methods act as wrappers for the "requireFiles" method when multiple files are required.

## Features ##

### Multiple files
The "files" argument/option in the methods *stylesheet()*, *js()*, and *requireFiles()* allows for multiple files to be required in one call. Furthermore, CSS and JavaScript files may be required at the same time, and will be separated automatically. Below are a few simple examples.

#### Example 1 - CSS files
A simple array of CSS files, with the media argument set.
```php
$backend = Requirements::backend();
$filePath = 'tkirequirements/tests/files';
$backend->stylesheets(
  array(
    "$filePath/RequirementsTest_a.css",
    "$filePath/RequirementsTest_b.css"
  ),
  'screen'
);
```
#### Example 2 - JavaScript files
A simple array of JavaScript files.
```php
$backend->js(
  array(
    "$filePath/RequirementsTest_a.js",
    "$filePath/RequirementsTest_b.js"
  )
);
```

#### Example 3 - Multiple files (uncombined)
A simple array of both CSS & JavasScript files.
```php
$backend = Requirements::backend();
$backend->requireFiles(
  array(
    "$filePath/RequirementsTest_a.css",
    "$filePath/RequirementsTest_a.js",
    "$filePath/RequirementsTest_b.js"
));
```
#### Example 4 - Multiple files (combined)
An associative array containing both CSS & JavasScript files.
The key is used to create the combined filename(s). The below code will produce "example4.css" and "example4.js".
```php
$backend->requireFiles(array(
  'example4' => array(
      "$filePath/RequirementsTest_a.css",
      "$filePath/RequirementsTest_a.js",
      "$filePath/RequirementsTest_b.js"
    )
  )
);
```
For further information:
* Refer to the method documentation for TkiRequirements_Backend::requireFiles, which describes more formats available for the $files argument.
* See the ["Examples"](#examples) section, which explains how you may view the output from the examples.

### Script location
String - (head|body|bottom)

The "location" argument/option allows developers to specify where each script or group of scripts are inserted into the document. It may be one of the following three locations:
* *head* - before the closing head tag (after the stylesheets).
* *body* - before the first script tag in the body (if present) or before the closing body tag.
* *bottom* - at the bottom before the closing body tag and after any body scripts.

#### Example 5 - location argument
Scripts inserted at each of the available locations.

```php
$backend->js("$filePath/RequirementsTest_a.js",'head');

$backend->js("$filePath/RequirementsTest_b.js",'body');

$backend->js(array(
  "$filePath/RequirementsTest_c.js",
  "$filePath/RequirementsTest_d.js"
),'bottom';
```

#### Example 6 - location argument for files and scripts
If you've ever wanted to include your JS vars in document head, your JS libraries in the body, and your custom script at the bottom, here's an easy way to do it:

```php
// JS vars in head
$backend->script("var A ='foo'; var B = 'bar';",null,'head');

// JS Libs
$backend->js(array(
  "$filePath/RequirementsTest_b.js",
  "$filePath/RequirementsTest_c.js"
),'body');

// JS script at bottom
$backend->script("alert(A + B)", null, 'bottom');
```

For existing Requirements calls, the location defaults to "body" (if *$write_js_to_body* is true). Consistency with the stock Requirements_Backend is maintained when it comes to the available class configuration properties:
* *$force_js_to_bottom* - if true, the location option is overridden, and all scripts are forced to the bottom (priority is still respected).
* *$write_js_to_body* - if false, the location option is overridden, and all scripts are forced into the head (priority is still respected).

### Prioritisation
Integer - (generally between 0-100)

The "priority" argument/option allows prioritisation of both CSS and JavaScript files. The higher the priority number, the greater the priority, just like the priority value previously used for Director rules. Existing Requirements calls are assigned a default priority of 50 (configurable with the $default_priority class property).

Scripts are sorted by priority within their respective assigned locations (head, body, and bottom), or globally if all scripts are forced into the head or to the bottom. It is recommended to correlate priority with location, in case you need to use an override to force all the scripts into the head or body, then your scripts will still be in the correct order.

#### Example 7 - priority option

```php
// Stock method - assigned default priority (50)
$backend->javascript("$filePath/RequirementsTest_c.js");

// Page script to be loaded after libs
$backend->js("$filePath/RequirementsTest_d.js",'body',30);

// JS Libs
$backend->js(array(
  "$filePath/RequirementsTest_a.js",
  "$filePath/RequirementsTest_b.js"
),'body',70);
```
The above example will result in the scripts being ordered *a*,*b*,*c*,*d* and placed in the body before any script tags in the template.


#### Example 8 - priority with location
In this example, our requirements need to loaded as follows:
* JS vars in the head
* JS libs in the head
* JS libs at the bottom of the document
* JS script at the bottom after the JS libs

We can make the requirement calls in any order, such as:
```php
// Page script to be loaded after libs
$backend->script("alert(A + B);", 'example8', 'bottom', 30);

// Body JS Libs
$backend->js(array(
  "$filePath/RequirementsTest_b.js",
  "$filePath/RequirementsTest_c.js"
), 'bottom', 40);

// Head JS libs
$backend->js("$filePath/RequirementsTest_a.js", 'head', 70);

// Head JS vars
$backend->script("var A ='foo'; var B = 'bar';",null,'head',80);

```
In addition, since we correlated the priority with location, if all scripts are forced into the head or to the bottom, they will still be ordered correctly.


## Examples
A simple controller is included to show the output (plain text) from the provided examples. Use the following route pattern to view the examples:

http://[domain]/tkirequirements/examples/[num]

Please note you must be logged in as an admin to view these routes.
